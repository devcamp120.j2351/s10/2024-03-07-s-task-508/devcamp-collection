//khai báo thư viện express
const express = require('express')

//khai báo router
const router = express.Router();

//import middleware
const courseMiddleware = require('../middlewares/course.middleware')

router.use(courseMiddleware.CourseCommon);

//khai báo các request
//get all course
router.get('/', courseMiddleware.GetAllCourse, (req, res) => {
    console.log('Get All Courses');
    res.json({
        message:'Get All Courses'
    })
})

//get a couse by id
router.get('/:courseid', courseMiddleware.GetCourseByID, (req, res) => {
    console.log('Get course by id');
    res.json({
        message:'Get course by id'
    })
})

//create new course
router.post('/', courseMiddleware.CreateCourse, (req, res) => {
    console.log('Create a course');
    res.json({
        message:'Create a course'
    })
})

//update course by id
router.put('/:courseid', courseMiddleware.UpdateCourseByID, (req, res) => {
    console.log('Update course by id');
    res.json({
        message:'Update course by id'
    })
})

//delete course by id
router.delete('/:courseid', courseMiddleware.DeleteCourseByID, (req, res) => {
    console.log('Delete course by id');
    res.json({
        message:'Delete course by id'
    })
})


module.exports = router;
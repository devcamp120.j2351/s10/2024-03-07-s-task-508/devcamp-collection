const ReviewCommon = (req, res, next) => {
    console.log(`Review Middleware: Time: ${new Date()} - Method: ${req.method} - URL: ${req.url}`);

    next();
}

const GetAllReview = (req, res, next) => {
    console.log('Get All Review Middleware');

    next();
}

const GetReviewByID = (req, res, next) => {
    console.log('Get A Review By ID Middleware');

    next();
}

const CreateReview = (req, res, next) => {
    console.log('Create New Review Middleware');

    next();
}

const UpdateReviewByID = (req, res, next) => {
    console.log('Update Review By ID Middleware');

    next();
}

const DeleteReviewByID = (req, res, next) => {
    console.log('Delete Review By ID Middleware');

    next();
}
module.exports = {
    ReviewCommon,
    GetAllReview,
    GetReviewByID,
    CreateReview,
    UpdateReviewByID,
    DeleteReviewByID
}